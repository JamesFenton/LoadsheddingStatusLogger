# Loadshedding status logger

A simple python tool to log the loadshedding status to a mongo database.

The connection string must set as an environment variable with the name `LoadsheddingStatusLoggerDb`

### Dependencies
* `pip install pymongo`
* `pip install requests`