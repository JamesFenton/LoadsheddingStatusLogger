from pymongo import MongoClient
import datetime
import requests
import os
import sys

def get_status():
	"""Gets the current loadshedding status"""
	r = requests.get('http://loadshedding.eskom.co.za/LoadShedding/GetStatus')
	if r.status_code != 200:
		raise Exception("Could not connect to eskom.co.za")
	status = int(r.text) - 1
	return status

def log_status(collection, status):
	"""Logs the current loadshedding status to a collection in a database"""
	entry = {"time": datetime.datetime.utcnow(), "loadshedding": status}
	inserted_id = collection.insert_one(entry).inserted_id
	return inserted_id

if __name__ == "__main__":
    # get the db connection string from environment variables
	db_environment_variable_key = 'LoadsheddingStatusLoggerDb'
	try:
		connection_string = os.environ[db_environment_variable_key]
	except KeyError:
		raise Exception("Environment variable '{0}' not set".format(db_environment_variable_key))

    # connect to database
	client = MongoClient(connection_string)
	db = client['NoShedDb-Dev']
	collection = db['statuslogs']

    # get the status
	status = get_status()

    # log the status
	inserted_id = log_status(collection, status)
	print("Inserted status {0} with id {1}".format(status, inserted_id))